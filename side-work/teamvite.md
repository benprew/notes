# Teamvite

Architecture Notes
------------------
* Teams should be in a division as part of a season, not as a single value
  * Should team names also be tied to a season?
* Games should also be part of a season


landing page: http://www.teamvite.com
-------------------------------------
* needs stock images, and wordsmithing

player home page: http://www.teamvite.com/player?id=117
-------------------------------------------------------
* needs UI/UX design, elements appear disjointed and a little haphazard
* needs to allow user to "edit" info
* display user info? (email, phone, only to user)

menu navigation
---------------
* stark black/white coloring is too harsh
* menu items are confusing, need to have menu for facility managers/owners vs. regular players
* needs logo

facility manager homepage
-------------------------
* needs design/layout/UI/UX

season create/edit: http://www.teamvite.com/season/edit?season_id=4
-------------------------------------------------------------------
* needs design/layout/UI/UX
* setting up a new season should be broken into separate steps, to make it easier to understand.  The steps are:
  1. set season starting date and name
  2. set facility hours and holidays
  3. add teams that are playing in the season
  4. edit team info for requested days off and preferred days to play
