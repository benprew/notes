---
title: Vegan Thai Curry Vegetables Recipe
---

### Ingredients

*   One 13 1/2-ounce can coconut milk (do not shake can)
*   ½ cup vegetable stock
*   4 teaspoons soy sauce
*   4 teaspoons palm sugar or brown sugar
*   6 tablespoons Thai green curry paste, like Thai Taste, Maesri, and Thai Kitchen, none of which have shrimp or fish paste
*   ½ cup diced (1/2 inch) onion
*   ⅔ cup diced (1/2 inch) red bell pepper
*   ⅔ cup diced (1/2 inch) zucchini or other summer squash
*   ⅔ cup diced (1/2 inch) peeled sweet potato
*   ⅔ cup sliced bamboo shoots, rinsed and drained
*   1 cup green beans, trimmed and cut into 1 1/2 inch lengths
*   ⅔ cup diced (1/2 inch) Asian eggplant
*   1 lime
*   8 large basil leaves, cut into thin chiffonade

[Email Grocery List](mailto:?subject=NYT%20Cooking:%20Vegan%20Thai%20Curry%20Vegetables%20-%20Grocery%20List&body=One%2013%201/2-ounce%20can%20coconut%20milk%20(do%20not%20shake%20can)%0D%0A1/2%20cup%20vegetable%20stock%0D%0A4%20teaspoons%20soy%20sauce%0D%0A4%20teaspoons%20palm%20sugar%20or%20brown%20sugar%0D%0A6%20tablespoons%20Thai%20green%20curry%20paste,%20like%20Thai%20Taste,%20Maesri,%20and%20Thai%20Kitchen,%20none%20of%20which%20have%20shrimp%20or%20fish%20paste%0D%0A1/2%20cup%20diced%20(1/2%20inch)%20onion%0D%0A2/3%20cup%20diced%20(1/2%20inch)%20red%20bell%20pepper%0D%0A2/3%20cup%20diced%20(1/2%20inch)%20zucchini%20or%20other%20summer%20squash%0D%0A2/3%20cup%20diced%20(1/2%20inch)%20peeled%20sweet%20potato%0D%0A2/3%20cup%20sliced%20bamboo%20shoots,%20rinsed%20and%20drained%0D%0A1%20cup%20green%20beans,%20trimmed%20and%20cut%20into%201%201/2%20inch%20lengths%0D%0A2/3%20cup%20diced%20(1/2%20inch)%20Asian%20eggplant%0D%0A1%20lime%0D%0A8%20large%20basil%20leaves,%20cut%20into%20thin%20chiffonade%0D%0A-----%0D%0AView%20Vegan%20Thai%20Curry%20Vegetables:%20https://cooking.nytimes.com/recipes/1015694-vegan-thai-curry-vegetables?grocerylist%0D%0AFor%20more%20recipes,%20visit:%20https://cooking.nytimes.com)

### Preparation

1.  Open the can of coconut milk without shaking it. Spoon 6 tablespoons of the coconut cream from the top of the can into a medium saucepan. Pour remaining contents of can into a medium bowl, and mix well. In a medium bowl, combine vegetable stock, soy sauce, and palm or brown sugar. Stir until the sugar is dissolved.
2.  Place saucepan of coconut cream over medium-high heat until it begins to bubble. Add curry paste and reduce heat to medium-low. Stir constantly until very fragrant, about 3 minutes; adjust heat as needed to prevent burning. Add onion, red pepper, zucchini, sweet potato, bamboo shoots, green beans, and eggplant. Stir until vegetables are hot, 2-3 minutes. Stir in coconut milk, bring the mixture to a boil, and reduce heat to low. Simmer, uncovered, until the vegetables are tender, about 10 minutes.
3.  Add the soy sauce mixture and a generous squeeze of fresh lime juice to taste; you may use the juice of an entire lime. Stir and mix well. Add up to 1/4 cup water if the curry seems too thick.
4.  To serve, place the curry in a warm serving bowl and garnish with the basil chiffonade. If desired, serve over jasmine rice.
