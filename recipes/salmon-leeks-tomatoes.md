---
title: Sautéed Salmon With Leeks and Tomatoes Recipe
---

### Ingredients

*   4 skinless, boneless salmon fillets, about 1 1/2 pounds
*   Salt to taste
*   Freshly ground pepper to taste
*   1 leek, well trimmed
*   2 or 3 red ripe tomatoes, about 1 1/2 pounds
*   3 tablespoons olive oil
*   2 tablespoons lemon juice
*   2 tablespoons chopped fresh coriander or basil

[Email Grocery List](mailto:?subject=NYT%20Cooking:%20Saut%C3%A9ed%20Salmon%20With%20Leeks%20and%20Tomatoes%20-%20Grocery%20List&body=4%20skinless,%20boneless%20salmon%20fillets,%20about%201%201/2%20pounds%0D%0ASalt%20to%20taste%0D%0AFreshly%20ground%20pepper%20to%20taste%0D%0A1%20leek,%20well%20trimmed%0D%0A2%20or%203%20red%20ripe%20tomatoes,%20about%201%201/2%20pounds%0D%0A3%20tablespoons%20olive%20oil%0D%0A2%20tablespoons%20lemon%20juice%0D%0A2%20tablespoons%20chopped%20fresh%20coriander%20or%20basil%0D%0A-----%0D%0AView%20Saut%C3%A9ed%20Salmon%20With%20Leeks%20and%20Tomatoes:%20https://cooking.nytimes.com/recipes/6015-sauteed-salmon-with-leeks-and-tomatoes?grocerylist%0D%0AFor%20more%20recipes,%20visit:%20https://cooking.nytimes.com)

### Preparation

1.  The fish should be at room temperature. If there are any bones in the salmon, pull them out with tweezers and discard. Sprinkle the fish with salt and pepper.
2.  Trim off the stem ends of the leek. Cut the leek lengthwise into thin slices. Finely chop the leek. There should be about 1/3 cup.
3.  Drop the tomatoes into boiling water and let stand 9 to 12 seconds.
4.  Peel and remove the cores. Cut the flesh of the tomatoes into quarter-inch cubes. There should be about 1 1/4 cups.
5.  Heat 2 tablespoons of the oil in a nonstick skillet over moderately high heat and add the salmon pieces skinned side up. Cook 2 minutes and then turn the pieces. If you want the salmon medium rare, cook the fish 2 minutes longer. Transfer the fish to a warm platter.
6.  Add the remaining oil to the skillet. Add the tomatoes and leek, salt, pepper and lemon juice. Cook about 1 minute. Pour the mixture over the salmon and sprinkle with the coriander or basil.

![Get recipes, tips and special offers in your inbox.](//int.nyt.com/applications/cooking/static/images/recipe-detail-signup.jpg)
