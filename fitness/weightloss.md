April 2016
* take Cooper Test monthly/bi monthly

November 2015
* goal: work on flexibility - hacking your hamstrings
* goal: figure out some kind of light lifting routine - 3x week
  * doesn't interfere with soccer
  * just about getting the habit of lifting down
  * fitness center in the morning?
  * CJCC lifting?
  * join POINT?
* still taking max/biking to work most days

August 2015
* goal: start lifting again 1x week

* morning exercise kettle bell 3x week
* yoga 1x week
* goal met! taking max 4+ times/week

Match 2015
* down to 161!
* taking max/biking regularly!
* goal: start measuring waist and track
* need to workout regularly
* goal: end of summer squat 275

1/5
* goal: 2 days/week non-soccer workout
* goal: improve aerobic fitness
  * step test? Beep test?
* goal: take max/bike 2 days/week
* goal: lose belly fat
  * measure waist? Fat calipers?
* look into LA fitness boot camp class

12/18
* Your car is broken, start taking the MAX
* 3-days workout, morning MWF - Lifting Day, Skills Day, Cardio/Agility day
* Setup monthly weightloss reviews
* weigh myself every morning
