# Testing (Sandi Metz) #

* assert the direct public side effects
  * IE, the "message" sent to an external process

* outgoing command messages
  * expect to send (with expect in mock object)

Stubs
* stubs define context
* stub as needed (but zillions are a smell)
* avoid stubbing object under test
* do not make assertions about stubs!

Mocks
* mocks test behavior
* set expectations on mocks to test outgoing command messages
* one expectation per test
* do not stub using a mock!

Ensure test double stays in line with API
minitest requires that stubbed methods exist!

see quacky for requiring methods in mocks
