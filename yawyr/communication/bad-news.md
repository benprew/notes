---
title: Breaking Bad News
url: http://boz.com/articles/bad-news.html
---

More than a few times in my career I’ve had to break bad news to people. The project is getting shut down. The work isn’t meeting expectations. Facilities are tight and only going to get tighter. The list goes on.

I’ve seen a lot of talented and well-meaning leaders struggle to communicate effectively in these situations. It is tempting to fall back on the pantheon of defensive corporate-speak techniques to defray the emotional impact of bad news. Here are a few such tactics you've probably seen before or even used yourself:

1.  Deflect responsibility: “As our fearless leader said…” or “As everyone knows…” or “Due to circumstances out of our control…” etc.
2.  Reframe it as good news: “Good news! <bad news> is actually great because reasons…”
3.  [Criticize skeptics](https://en.wikipedia.org/wiki/No_true_Scotsman): “If you want the best thing for the company, get on board”
4.  Bury the lead: “We’re making some routine changes to our health insurance policy…\[3 paragraphs later\]…we are canceling your health insurance.”
5.  Drown it in details: “Here are so many words that it's hard to tell exactly what changed.”
6.  Prescribe a response. "Thanks for being so understanding about this."

I understand the instinct to approach bad news like this. No one likes to be the bearer of bad news. Selfishly, we fear the accompanying reaction and how it may impact our own emotions. And while that's understandable it is ultimately self defeating. If you fail to show proper respect for the people you are talking to you might accidentally:

1.  Permanently undermine the credibility of leadership
2.  Insult the intelligence of your audience.
3.  Create an incentive for the audience to see themselves as separate.
4.  Delay awareness and understanding of ostensibly important news.
5.  Confuse people.

Speaking simply, it just doesn’t work.

The bad news is almost always a smaller issue than what happens when you don't take responsibility for it. That's the worst possible outcome.

It's best to take a straightforward approach: “Here is the change. Here is the negative impact. Here is why we still believe in it. Questions are welcome.” If you're direct, factual, and empathetic, you will still suffer the after affects of having to share bad news, but the relationships you have with your team will remain intact.

Published August 29, 2018

* * *
