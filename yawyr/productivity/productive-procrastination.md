---
title: Productive Procrastination: How to Get More Done by Procrastinating on Purpose | Nick Wignall
url: https://nickwignall.com/productive-procrastination/
---

**“Goddamn it, Nick! Stop being so productive.”**

My buddy likes to say this in the morning when he walks into work at 8:30 am and sees me dutifully typing away on my computer. He knows that I’ve been in that same spot for at least the last two hours, [being productive since 6:00 am](http://nickwignall.com/the-secret-to-productive-mornings/), well before our actual day job begins at 9:00.

And it’s true—**I’m a pretty productive guy.** Before I start seeing clients at 9:00 am each morning, I’ve usually done several hours worth of work on my own projects and hobbies. In fact, I’m writing this story now on a Wednesday morning at 6:45 in my office before work.

And while most people see me being productive and looking like I’m getting a lot done, what they don’t see is how much I [procrastinate](http://nickwignall.com/the-4-causes-of-procrastination/). And let me tell you, **I procrastinate A LOT!**

Here’s how a typical “productive morning” looks for me where my goal is to, say, finish a draft of a new Medium article:

*   Sit down at my desk and open up my computer.
*   **Procrastinate for 10 minutes.**
*   Start my pre-work ritual and write for 30 minutes.
*   **Procrastinate for 30 minutes.**
*   Get back into my article and write for an hour.
*   **Procrastinate for 45 minutes.**
*   Think about writing a little more. Decide it’s not worth it because I only have 15 minutes.
*   **Procrastinate for 14 minutes.**

In three hours, I got _maybe_ an hour and a half of writing done. Which means I only managed to use 50% of my time on that article. Hardly a glowing example of epic efficiency and productivity.

Or was it…?

Procrastination and Productivity are Not Opposites
--------------------------------------------------

“The most impressive people I know are all terrible procrastinators.” — Paul Graham in [Good and Bad Procrastination](http://www.paulgraham.com/procrastination.html)

I struggle with the tendency to procrastinate at least as much as most people.

The difference is, I don’t fight it. In fact, **I use my urge to procrastinate to be more productive.**

Before I explain my own method of productive procrastination, let’s look at what procrastination looks like for most people:

*   Think about working.
*   Immediately feel the urge to procrastinate.
*   Start beating themselves up with [a bunch of negative self-talk](http://nickwignall.com/rethinking-mental-math/) for wanting to procrastinate (_I’m such a procrastinator, why can’t you just stay focused?_)
*   Feel badly, including a bunch of negative emotions like shame and disappointment, _on top of_ the already-strong urge to procrastinate.
*   Procrastinate on something emotionally numbing.

**Most people don’t procrastinate to avoid work; they procrastinate to avoid the negative feelings that come from misinterpreting the urge to procrastinate.**

The initial urge to procrastinate is not the problem. It’s all the other [negative emotion that we heap on ourselves](http://nickwignall.com/your-emotional-brain/) by thinking of procrastination as a bad thing.

But let’s get back to me 😆

Here’s [my morning writing routine again](http://nickwignall.com/seinfeld-method-deep-work/), but this time I’ve filled in the details of what the procrastination actually looks like:

*   Sit down at my desk and open up my computer.
*   Procrastinate for 10 minutes… **By reading and commenting on Medium stories written by friends.
    **
*   Start my pre-work ritual and write for 30 minutes.
*   Procrastinate for 30 minutes… **By playing around with a new website design mockup in Sketch.
    **
*   Get back into my article and write for an hour.
*   Procrastinate for 45 minutes… **By reading a new book.
    **
*   Think about writing a little more. Decide it’s not worth it because I only have 15 minutes.
*   Procrastinate for 14 minutes… **By reading Hacker News or something else online.
    **

Even though my procrastination activities aren’t directly helping me write my article, they are helping me be productive, at least in the long run.

*   When I read and comment on friends’ Medium articles, I’m [cultivating these relationships](https://medium.com/swlh/6-honest-lessons-after-6-months-writing-on-medium-and-1-600-new-followers-44ede785c9ae) and increasing the chances that other people share my writing more widely when I publish.
*   When I play around with a new website design, I’m working out lots of little decisions and preferences, so that when the time comes to re-do my website, it’s quick and easy because I’ve already done a lot of the work via countless 30-minute productive procrastination sessions in Sketch.
*   When I read a new book, I’m gathering information for new articles and absorbing new ideas.
*   Even reading Hacker News or some other blog post is potentially useful because it’s where I find a lot of the articles that I share in [my weekly newsletter](http://nickwignall.com/newsletter/).

But even more importantly, by “rolling with” rather than fighting my urge to procrastinate, I avoid all the guilt and shame and truly counter-productive procrastination (binge-watching Netflix for half a day) that comes from viewing procrastination as something bad and a major character flaw.

**By procrastinating about _**the right things**_ in _**the right way**_, you’re never really procrastinating.**

If you’d like to try your hand at productive procrastination, here are three practical strategies to get you started.

* * *

STRATEGY 1: Stop Being an A$$hole to Yourself
---------------------------------------------

“I use procrastination as a guide from my inner self.” — Nassim Taleb

**Most peoples’ problems with procrastination begin and end with being jerks… to themselves.** Specifically, they talk a lot of trash to themselves in their own minds whenever they find themselves with the urge to procrastinate:

*   _God, why can’t I just focus?!_
*   _There I go again, getting distracted._
*   _I wish I wasn’t such a procrastinator…_
*   _If only I could work more consistently like Sandra._
*   _I’m never going to finish if I keep…_

The problem with all this self-smack-talk is that it makes us feel badly about ourselves, typically in the form of guilt or shame.

Imagine if you had an evil twin who walked around by your side constantly whispering in your ear about how terrible of a person you were and how you’ll always suck and never make anything of yourself?

It would feel awful to have someone saying things like that to us 24/7, right? Even if we knew intellectually that it wasn’t true, it would start to get to us if we were constantly bombarded with it.

And yet, a lot of us do this to ourselves, often without even realizing it. People who struggle with procrastination tend to do it a lot.

If this sounds like you, **the first step is to gain some awareness about how you talk to yourself when you feel the urge to procrastinate.** Notice what your default inner voice says. Then write it down.

By getting that voice out onto paper and into the real world, you create distance on it and can begin to see it for what it is: Not some intrinsic part of your personality, but [a mental habit](http://nickwignall.com/your-daddy-issues-are-really-daddy-habits/).

Maybe it’s something that’s been reinforced over years or even decades, but it’s also something that can be undone with some awareness and practice.

Once you’ve written down what your mind tends to say, for each thought, **come up with a handful of alternative thoughts**.

For example, suppose I notice that whenever I feel the urge to procrastinate, the first thought that runs through my mind is: _Damn, I wish I wasn’t such a procrastinator._

Some alternative thoughts might be:

*   _I do tend to procrastinate a lot with my writing, but I’m really disciplined about lots of other things like going to the gym and meal prepping._
*   _I guess I haven’t always struggled so much with procrastination It’s only been since starting my new job._
*   _Just because I feel the urge to procrastinate doesn’t mean that’s who I am or that I can’t also be productive._

### Takeaway

**Most forms of truly counter-productive procrastination (e.g. binge-watching Netflix or YouTube videos for 3 hours) are a result of us trying to escape from or avoid the shame that arises from our own overly-negative self-talk.** If we can identify and begin to modify this self-talk, we’ll feel less shame and negative emotion and therefore be less likely to engage in such mindless and unproductive forms of procrastination.

* * *

STRATEGY 2: Procrastinate Consistently
--------------------------------------

“You should never fight the tendency to procrastinate.” — Marc Andreessen in [The Pmarca Guide to Personal Productivity](https://pmarchive.com/guide_to_personal_productivity.html)

**When I allow myself to regularly procrastinate in small ways, I almost never procrastinate in big ways.**

I suppose there are some people out there who are capable of massive amounts of focused concentration on a single task, day in and day out. But I’m certainly not that type of person, nor do I want to be.

I’m interested in and enjoy lots of things. And unsurprisingly, my brain gets a little tired of focusing on one thing for too long and wants to mix it up.

In my experience, when you judiciously indulge this tendency from time to time, allowing your [natural curiosity](http://nickwignall.com/5-ways-to-boost-your-creativity-with-divergent-thinking/) to wander where it will, you avoid the major _need_ to procrastinate in a big way.

In other words, **look at procrastination** not **as an inherent character flaw but an expression of a natural and good human desire for variety and curiosity.**

And if you make time for this natural curiosity regularly and in small doses, rarely does it get pent up to the point that it results in one of those all-day Netflix binges that leave you in a comma, drooling off the side of your couch with Dorito crumbs all over your shirt.

### Takeaway

**Re-frame procrastination as a natural desire for variety and curiosity in your life**. If you give this natural curiosity regular outlets throughout your days and weeks, it won’t need to blow up into major procrastination.

* * *

STRATEGY 3: Cultivate Interests that Synergize with Your Work
-------------------------------------------------------------

“Virtually all procrastinators have excellent self-deceptive skills.” — John Perry in [Structured Procrastination](http://www.structuredprocrastination.com/)

**All great procrastinators have one thing in common: They procrastinate with productive things.**

Like I explained above, one of my favorite ways to procrastinate is by reading. When I’m tired of writing and need a break/distraction, I always have a handful of books close by that I’m interested in and find enjoyable.

Reading isn’t a direct and immediate productivity benefit in terms of the specific article I’m working on. But it is a long-term productivity benefit because it exposes me to [new ideas](http://nickwignall.com/5-ways-to-boost-your-creativity-with-divergent-thinking/). And if you’ve ever done any [consistent writing](http://nickwignall.com/seinfeld-method-deep-work/), you know how important it is to have a steady stream of new, interesting ideas to write about.

In other words, reading is a great way to procrastinate on my writing because they have a _[synergistic relationship](https://en.wikipedia.org/wiki/Synergy)._ Not only does reading give me a break from my writing, it also improves my writing and makes it easier to keep writing by steadily supplying new ideas.

One of my favorite writers on procrastination, Nilz Salzgeber, has another great example of this principle (procrastinating on writing via snorkeling) in [his own piece on productive procrastination](https://www.njlifehacks.com/productive-procrastination/).

If you want to get started with productive procrastination, it’s important to **cultivate a set of activities that are enjoyable, interesting, and provide healthy distractions and relief from your primary work**. But these activities should also contribute to or support your long-term productivity somehow, even if it’s in a very indirect way and over a long time period.

### Takeaway

By [cultivating hobbies and interests](http://nickwignall.com/how-to-find-a-side-project-youre-passionate-about-according-to-psychology/) that are at least indirectly supportive of your primary work, anytime you choose to procrastinate you’ll be engaging in productive procrastination.

* * *

Wrapping Up
-----------

The core idea behind productive procrastination is that we’d all be a lot happier and more productive if we thought a little differently about what procrastination is and what it means.

**Rather than viewing procrastination as a character flaw and something to be squashed, we ought to look at it as a natural desire we all have to diversify our work and interests.**

When we can re-frame it this way, it becomes much easier to harness procrastination and channel it to productive ends.

* * *

### What to Read Next

If you enjoyed this article, you might also like:

*   [The 4 Causes of Procrastination](http://nickwignall.com/the-4-causes-of-procrastination/)
*   [The Secret to Productive Mornings is to Make Them Easier (Not Earlier)](http://nickwignall.com/the-secret-to-productive-mornings-is-to-make-them-easier-not-earlier/)
*   [Nobody Lacks Motivation—We’re Just Motivated Toward the Wrong Things](http://nickwignall.com/nobody-lacks-motivation/)
