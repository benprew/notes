# Questions to ask potential employers/interviewers

## For co-workers

1. Do you know what is expected of you? (If the employee answers with a four or a five, this self-score would indicate that he or she knows the goal, how it is measured, and how he or she plans to reach the goal. An answer of one indicates that the employee does not know the goal or objective or how it is measured and has no idea how to reach it.)
2. Do you have the right materials and equipment to do your work right?
3. At work, do you have the opportunity to do what you do best every day?
4. In the last seven days, have you received recognition or praise for doing good work?
5. Does you supervisor, or someone at work, seem to care about me as a person?
6. Is there someone at work who encourages your development?
7. At work, do your opinions seem to count?
8. Does the mission/purpose of the company make me feel that your job is important?
9. Are your co-workers committed to doing quality work?
10. Do you have a best friend at work?
11. In the last six months, has someone at work talked to you about your progress?
12. This last year, have you had opportunities at work to learn and grow?
