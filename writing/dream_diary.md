dreams

Night of 10/5

Note: I could feel the rest of the dream just outside my memory, but I couldn't recall it.

I enter into a bathroom, possibly running away from something or someone.  I have a feeling that the layout of the house and location of the bathoom makes it the house I grew up in.  But the bathroom is different, the cabinets are all dark wood and there's dark wood around the mirror.  The house I grew up in was built in the 70s, but this bathroom has an older feel to it, as if it had been around longer and had design traits from several eras, from the early 20th century to modern time.

I look in the mirror and start making faces and realize that when I scruch up my face, I look a lot like a former co-worker, I can hear his voice in mine.  He has fair, clean-shaven skin and light strawberry-colored hair, however my visage has hair that is dark, almost black, forming a scraggly beard.  I alternate scrunching up my face and relaxing it and after several iterations, I realize that I am ugly, that I was never attractive and I don't know how someone could be attracted to me.  In my dream I move through the emotions quickly, fear, anger, resentment and finally acceptance.  Scrunching up my face
has caused my eyes to become large and bulbous, I am a hairy Gollum.

I wake up and as I move my hand up to my face, I am relieved to feel my clean-shaven face.

Moral: I believe that all my dreams are attempts by my
subconsciousness to prepare me for something.  In this dream, I feel like this dream was preparing me for growing older, and accepting that as an older man, I'm not going to be as attractive as I was as a younger man.  And maybe to help me not focus on my physical looks so much.  I should accept who I am and what I look like and not spend my life trying to live up to some vague, media-influenced notion of what attractive men look like.
